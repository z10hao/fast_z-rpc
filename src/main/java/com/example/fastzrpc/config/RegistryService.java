package com.example.fastzrpc.config;

import com.alibaba.fastjson.JSON;
import com.example.fastzrpc.ZookeeperServerConfigProperties;
import com.example.fastzrpc.core.ServiceRegistry2;
import com.example.fastzrpc.dto.ServiceInstanceInfo;
import com.example.fastzrpc.utils.IPUtil;
import com.example.fastzrpc.utils.YamlReader;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.ZooDefs;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.data.Stat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;


/**
 * @author zhangt
 * @create 2022-09-11
 */
public class RegistryService implements ServiceRegistry2 {
    private final Logger logger = LoggerFactory.getLogger(RegistryService.class);
    private final ZooKeeper zooKeeper;
    private final ZookeeperServerConfigProperties configProperties;

    public RegistryService(ZooKeeper zooKeeper) {
        this.zooKeeper = zooKeeper;
        this.configProperties = ZookeeperServerConfigProperties.config();
    }
/**
 * 会在zookeeper中创建以下znode结构
 * /namespace/serviceName/serviceName0000000000
 *                       /serviceName0000000001
 *                       /serviceName0000000002
 * @return 注册成功返回true
 */
    @Override
    public boolean registry() {
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream(); ObjectOutputStream oos = new ObjectOutputStream(baos)) {
            ServiceInstanceInfo info = generateServiceInfo();
            oos.writeObject(info);
            ensureZNodeExist(configProperties.getNamespace(), info.getServiceName(), "zkWatcher");
            Stat exists3 = zooKeeper.exists(configProperties.getNamespace() + "/" +
                    info.getServiceName() + "/zkWatcher", false);
            if(exists3==null){
                this.zooKeeper.create(configProperties.getNamespace() + "/" +
                                info.getServiceName() + "/" + info.getServiceName()+ "/zkWatcher",
                        JSON.toJSONString(new ServiceInstanceInfo(
                                "wather","",1,"watcher",null
                        )).getBytes() ,
                        ZooDefs.Ids.OPEN_ACL_UNSAFE,
                        CreateMode.EPHEMERAL);
            }

        }
        catch (IOException | InterruptedException | KeeperException e) {
            logger.error(e.getMessage(), e);
            return false;
        }
        return true;
    }
    private void ensureZNodeExist(String namespace, String serviceName, String methodName) throws KeeperException, InterruptedException {

        Stat exists = zooKeeper.exists(namespace, false);
        if (exists == null) {
            logger.info("zookeeper namespace:" + namespace + "不存在，开始创建...");
            String s = zooKeeper.create(namespace, null, ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
            logger.info("zookeeper namespace:" + s + "已创建！");
        }
        Stat isExist = zooKeeper.exists(namespace + "/" + serviceName, false);
        if (isExist == null) {
            logger.info("zookeeper service Znode:" + serviceName + "不存在，开始创建...");
            String s = zooKeeper.create(namespace + "/" + serviceName, null,
                    ZooDefs.Ids.OPEN_ACL_UNSAFE,
                    CreateMode.PERSISTENT);
            logger.info("zookeeper service Znode:" + s + "已创建！");
        }
        Stat isExist2 = zooKeeper.exists(namespace + "/" + serviceName+"/"+methodName, false);
        if (isExist2 == null) {
            logger.info("zookeeper service Znode:" + serviceName + "不存在，开始创建...");
            String s = zooKeeper.create(namespace + "/" + serviceName+"/"+methodName, null,
                    ZooDefs.Ids.OPEN_ACL_UNSAFE,
                    CreateMode.PERSISTENT);
            logger.info("zookeeper service Znode:" + s + "已创建！");
        }
    }

    private ServiceInstanceInfo generateServiceInfo() {
        //ApplicationProperties config = ApplicationProperties.config();
        return new ServiceInstanceInfo( YamlReader.instance.getValueByKey("zRpc.protocol.name").toString(),
                IPUtil.getIpAddress(),
            Integer.parseInt(  YamlReader.instance.getValueByKey("zRpc.protocol.port").toString()),"watcher" ,null );
    }
}
