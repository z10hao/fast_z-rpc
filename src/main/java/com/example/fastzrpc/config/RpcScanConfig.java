package com.example.fastzrpc.config;

import com.example.fastzrpc.annotation.RpcClass;
import com.example.fastzrpc.annotation.RpcService;
import com.example.fastzrpc.dto.RpcBeanInfoDTO;
import com.example.fastzrpc.dto.RpcMethodInfoDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * @author zhangt
 * @create 2022-09-09
 */
@Slf4j
@Component
public class RpcScanConfig implements BeanPostProcessor {
    public RpcBeanInfoDTO rpcBeanInfoDTO;
    @Autowired
    ServiceRegistryConfig serviceRegistryConfig;
    //bean初始化方法调用前被调用
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        if (bean.getClass().isAnnotationPresent(RpcClass.class)) {
            log.info("[{}] is annotated with  [{}]", bean.getClass().getName(), RpcClass.class.getCanonicalName());
            RpcClass rpcClass = bean.getClass().getAnnotation(RpcClass.class);
            rpcBeanInfoDTO=new RpcBeanInfoDTO();
            rpcBeanInfoDTO.setBeanName(bean.getClass().getName());
            rpcBeanInfoDTO.setBeanGroup(rpcClass.group());
        }
        return bean;
    }
    //bean初始化方法调用后被调用
    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if (bean.getClass().isAnnotationPresent(RpcClass.class)) {
        Class<?> targetClass = bean.getClass();
       // Field[] declaredFields = targetClass.getDeclaredFields();
        Method[] methods= targetClass.getDeclaredMethods();
        List<RpcMethodInfoDTO> rpcMethodInfos=new ArrayList<>();
        RpcMethodInfoDTO rpcMethodInfoDTO=new RpcMethodInfoDTO();
            for (Method method : methods) {
                RpcService rpcService = method.getAnnotation(RpcService.class);
                if(rpcService!=null){
                    Class<?>[] types=   method.getParameterTypes();
                    rpcMethodInfoDTO.setMethodName(method.getName());
                    rpcMethodInfoDTO.setParameterTypes(types);
                }
                rpcMethodInfos.add(rpcMethodInfoDTO);
            }
        rpcBeanInfoDTO.setBeanMethods(rpcMethodInfos);
        if(rpcBeanInfoDTO!=null) {
            serviceRegistryConfig.push(rpcBeanInfoDTO);
        }
        }
        return bean;
    }
}
