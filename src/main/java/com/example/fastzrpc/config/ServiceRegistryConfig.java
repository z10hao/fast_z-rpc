package com.example.fastzrpc.config;


import com.alibaba.fastjson.JSON;
import com.example.fastzrpc.ZookeeperServerConfigProperties;
import com.example.fastzrpc.core.ServiceRegistry;
import com.example.fastzrpc.dto.RpcBeanInfoDTO;
import com.example.fastzrpc.dto.RpcMethodInfoDTO;
import com.example.fastzrpc.dto.ServiceInstanceInfo;
import com.example.fastzrpc.utils.IPUtil;
import org.apache.zookeeper.*;
import org.apache.zookeeper.data.Stat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.net.InetAddress;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.concurrent.CountDownLatch;


/**
 * @author zhangt
 * @create 2022-09-09
 */
@Component
public class ServiceRegistryConfig  implements ServiceRegistry,  Watcher {
    private static Logger logger = LoggerFactory.getLogger(ServiceRegistryConfig.class);
    private static CountDownLatch latch = new CountDownLatch(1);
    ZookeeperServerConfigProperties zkConfig = ZookeeperServerConfigProperties.config();
    ServiceInstanceInfo serviceInstanceInfo;
    public ServiceRegistryConfig() {
    }
    public  void push(RpcBeanInfoDTO rpcBeanInfoDTO){
        List<RpcMethodInfoDTO> rpcMethods=rpcBeanInfoDTO.getBeanMethods();
        for(RpcMethodInfoDTO rpcMethodInfoDTO : rpcMethods){
            String beanGroup=rpcBeanInfoDTO.getBeanGroup();
            String beanNameFull=rpcBeanInfoDTO.getBeanName();
            String[] baenNames=beanNameFull.split("\\.");
            String beanName=baenNames[baenNames.length-1];
            String[] nodes={beanGroup,beanName,rpcMethodInfoDTO.getMethodName(),beanNameFull};
           register(nodes,rpcMethodInfoDTO.getParameterTypes());
        }
    }
    @Override
    public  void register(String serviceName ,String beanGroup,String BeanName,String beanNameFull,Class<?>[] parameterTypes) {
        /**
         * 在zk创建根节点path,在根节点下创建临时子节点用于存放服务ip和端口
         */
        try {
            String path = zkConfig.getNamespace() ;
            ZooKeeper zooKeeper = new ZooKeeper(zkConfig.getServerAddress(),zkConfig.getSessionTimeout(),(watchedEvent) -> {});
            System.out.println(zooKeeper);
            Stat exists = zooKeeper.exists(path, false);
            //先判断服务根路径是否存在
            if (exists == null){
                zooKeeper.create(path,"1".getBytes(), ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
            }
            Stat exists2 = zooKeeper.exists(path+"/"+beanGroup, false);
            //先判断服务根路径是否存在
            if (exists2 == null){
                zooKeeper.create(path+"/"+beanGroup,"1".getBytes(), ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
            }
            //将服务的ip和端口作为临时带序号的子节点
            Stat exists3 = zooKeeper.exists(path+"/"+beanGroup+"/"+BeanName, false);
            //先判断服务根路径是否存在
            if (exists3 == null){
                zooKeeper.create(path+"/"+beanGroup+"/"+BeanName, "1".getBytes(), ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
            }
            Stat exists4 = zooKeeper.exists(path+"/"+beanGroup+"/"+BeanName+"/"+serviceName, false);
            serviceInstanceInfo=new ServiceInstanceInfo(serviceName,IPUtil.getIpAddress(),Integer.parseInt(zkConfig.getzRpcPort()),beanNameFull,parameterTypes);
            //先判断服务根路径是否存在
            if (exists4 == null){
                zooKeeper.create(path+"/"+beanGroup+"/"+BeanName+"/"+serviceName, JSON.toJSONString(serviceInstanceInfo).getBytes(),ZooDefs.Ids.OPEN_ACL_UNSAFE,CreateMode.EPHEMERAL);
            }else {
                zooKeeper.setData(path+"/"+beanGroup+"/"+BeanName+"/"+serviceName, JSON.toJSONString(serviceInstanceInfo).getBytes(),-1);
            }
             logger.info(serviceName+"服务注册成功");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public  void register(String[] nodes ,Class<?>[] parameterTypes) {
        try {
            String path = zkConfig.getNamespace() ;
            ZooKeeper zooKeeper = new ZooKeeper(zkConfig.getServerAddress(),zkConfig.getSessionTimeout(),(watchedEvent) -> {});
            Stat exists = zooKeeper.exists(path, false);
            if (exists == null)
                zooKeeper.create(path,"1".getBytes(), ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
            int i=0;
            StringBuffer nodePath=new StringBuffer();
            nodePath.append(path);
            while (i < nodes.length-1){
                nodePath.append("/").append(nodes[i]);
                Stat exist = zooKeeper.exists(nodePath.toString(), false);
                if(i==nodes.length-2){
                    serviceInstanceInfo=new ServiceInstanceInfo(nodes[i],IPUtil.getIpAddress(),Integer.parseInt(zkConfig.getzRpcPort()),nodes[i+1],parameterTypes);
                    if (exist == null)
                        zooKeeper.create(nodePath.toString(), JSON.toJSONString(serviceInstanceInfo).getBytes(),ZooDefs.Ids.OPEN_ACL_UNSAFE,CreateMode.EPHEMERAL);
                    else
                        zooKeeper.setData(nodePath.toString(), JSON.toJSONString(serviceInstanceInfo).getBytes(),-1);
                }else{
                    if (exist == null)
                        zooKeeper.create(nodePath.toString(),"1".getBytes(), ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
                }
                i++;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void process(WatchedEvent event) {
        if (event.getState() == Event.KeeperState.SyncConnected)
            latch.countDown();
    }
}
