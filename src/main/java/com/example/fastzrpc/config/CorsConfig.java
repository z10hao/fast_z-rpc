package com.example.fastzrpc.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author zhangt
 * @create 2023-01-31
 * 通过 CORS 来解决跨域问题，只不过之前我们是在 XML 文件中配置 CORS ，
 * 现在可以通过实现WebMvcConfigurer接口然后重写addCorsMappings方法解决跨域问题
 */
@Configuration
public class CorsConfig implements WebMvcConfigurer {
    /**
     * Configure cross origin requests processing.
     * @since 4.2
     */
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("/**")
                .allowCredentials(true)
                .allowedMethods("GET", "POST", "PUT", "DELETE", "OPTIONS")
                .maxAge(3600);
    }

}
