package com.example.fastzrpc.config;

import com.alibaba.fastjson.JSONObject;
import com.example.fastzrpc.ZookeeperServerConfigProperties;
import com.example.fastzrpc.core.ServiceDiscover;
import com.example.fastzrpc.dto.ServiceInstanceInfo;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.WatchedEvent;

import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.data.Stat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author zhangt
 * @create 2022-09-11
 */
public class ServiceDiscoverConfig implements ServiceDiscover {
    private final Logger logger = LoggerFactory.getLogger(ServiceDiscoverConfig.class);
    private final ZookeeperServerConfigProperties properties;
    private final ZooKeeper zooKeeper;
    private final ConcurrentHashMap<String, Map<String, Map<String, ServiceInstanceInfo>>> registry;

    public ServiceDiscoverConfig(ZookeeperServerConfigProperties properties, ZooKeeper zooKeeper) {
        this.properties = properties;
        this.zooKeeper = zooKeeper;
        this.registry = new ConcurrentHashMap<>(4);
    }

    @Override
    public void discovery(WatchedEvent e) throws KeeperException, InterruptedException {
        int type = e.getType().getIntValue();
        if (type == -1)
            return;
        logger.info("注册信息发生变化，type=" + type + ",path=" + e.getPath());
        if (type == 1) {//节点创建（有新的服务注册）
            logger.info("有新的服务注册，添加服务信息到注册表...");
            String path = e.getPath();
            ServiceInstanceInfo serviceInstanceInfo = addServiceInfo(path);
            logger.info("已添加服务：" + serviceInstanceInfo);
        }
        else if (type == 2) {//节点删除（有服务断开）
            logger.info("有服务离线，从注册表删除服务信息...");
            String path = e.getPath();
            ServiceInstanceInfo serviceInstanceInfo = deleteServiceInfo(path);
            logger.info("已移除服务：" + serviceInstanceInfo);
        }
    }
    private ServiceInstanceInfo deleteServiceInfo(String path) {
        String[] split = path.split("/");
        if (split.length < 4)
            return null;
        String groupName = split[2];
        String classeName = split[3];
        String methodName = split[4];
        Map<String,Map<String, ServiceInstanceInfo>> map = registry.get(groupName);
        Map<String, ServiceInstanceInfo>  map2= map.get(classeName);
        ServiceInstanceInfo serviceInstanceInfo = map2.get(methodName);
        map.remove(methodName);
        return serviceInstanceInfo;
    }

    private ServiceInstanceInfo addServiceInfo(String path) throws KeeperException, InterruptedException {
        //例如：path=/service/serviceName/serviceName0000000000
        String[] split = path.split("/");
        if (split.length < 4)
            return null;
        //分割之后，serviceName就是第2个元素
        String groupName = split[2];
        String classeName = split[3];
        String methodName = split[4];
        Map<String, Map<String, ServiceInstanceInfo>> map = registry.get(groupName);
        if(map==null){
            map = new ConcurrentHashMap<>();
            registry.put(groupName,map);
        }
        Map<String, ServiceInstanceInfo>  map2= map.get(classeName);
        if(map2 == null){
            map2 = new ConcurrentHashMap<>();
            registry.get(groupName).put(classeName,map2);
        }
        byte[] data = zooKeeper.getData(path, false, new Stat());
        ServiceInstanceInfo serviceInstanceInfo = JSONObject.parseObject( new String(data),ServiceInstanceInfo.class) ;
        map2.put(methodName, serviceInstanceInfo);
        return serviceInstanceInfo;
    }

    public void flushRegistry() throws KeeperException, InterruptedException {
        List<String> serviceNameList = zooKeeper.getChildren(properties.getNamespace(), true);//获取group集合
        if (serviceNameList == null)
            return;
        for (String serviceName : serviceNameList) {
            Map<String, Map<String, ServiceInstanceInfo> > serviceList = registry.get(serviceName);
            if (serviceList == null)
                serviceList = new ConcurrentHashMap<>();
            List<String> serviceInstanceList = zooKeeper
                    .getChildren(properties.getNamespace() + "/" + serviceName,//获取group下bean集合
                            false);
            if (serviceInstanceList != null) {
                for (String serviceInstanceName : serviceInstanceList) {
                     Map<String, ServiceInstanceInfo> serviceList4= new ConcurrentHashMap<>();
                    List<String> methodList = zooKeeper
                            .getChildren(properties.getNamespace() + "/" + serviceName+ "/" + serviceInstanceName,//获取bean下method集合
                                    false);
                    if (methodList != null) {
                        for (String method : methodList) {
                            byte[] data = zooKeeper.getData(
                                    properties.getNamespace() + "/" + serviceName + "/" +
                                            serviceInstanceName+ "/" +
                                            method,
                                    false, new Stat());
                            ServiceInstanceInfo serviceInstanceInfo = JSONObject.parseObject( new String(data),ServiceInstanceInfo.class) ;
                            serviceList4.put(method, serviceInstanceInfo);
                        }
                    }
                    serviceList.put(serviceInstanceName,serviceList4);
//                    byte[] data = zooKeeper.getData(
//                            properties.getNamespace() + "/" + serviceName + "/" +
//                                    serviceInstanceName,
//                            false, new Stat());
//                    ServiceInstanceInfo serviceInstanceInfo = JSONObject.parseObject( new String(data),ServiceInstanceInfo.class) ;
//                    serviceList.put(serviceInstanceName, serviceInstanceInfo);
                }
            }
            registry.put(serviceName, serviceList);
        }
    }

    private ServiceInstanceInfo toObject(byte[] data) {
        try (
                ByteArrayInputStream bis = new ByteArrayInputStream(data);
                ObjectInputStream ois = new ObjectInputStream(bis)
        ) {
            return (ServiceInstanceInfo) ois.readObject();
        }
        catch (ClassNotFoundException | IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public ConcurrentHashMap<String, Map<String, Map<String, ServiceInstanceInfo>>> getRegistry() {
        return registry;
    }

}
