package com.example.fastzrpc.netty.client;

import com.example.fastzrpc.dto.ResponseResult;
import com.example.fastzrpc.utils.IPUtil;
import com.example.fastzrpc.utils.ObjectAndByte;
import com.example.fastzrpc.utils.YamlReader;
import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.util.CharsetUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;

/**
 * @author zhangt
 * @create 2022-09-11
 * NettyClient工具类
 * Netty客户端
 */
@Slf4j
public class NettyClientUtil {

    public static ResponseResult RpcNetty(Object msg,String ipAdress,Integer port) throws Exception {
        NettyClientHandler nettyClientHandler = new NettyClientHandler();
        EventLoopGroup group = new NioEventLoopGroup();
        Bootstrap bootstrap = new Bootstrap()
                .group(group)
                //该参数的作用就是禁止使用Nagle算法，使用于小数据即时传输
                .option(ChannelOption.TCP_NODELAY, true)
                .channel(NioSocketChannel.class)
                .handler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel socketChannel) throws Exception {
                        socketChannel.pipeline().addLast("decoder", new StringDecoder());
                        socketChannel.pipeline().addLast("encoder", new StringEncoder());
                        socketChannel.pipeline().addLast(nettyClientHandler);
                    }
                });
        try {
            ChannelFuture future = bootstrap.
                    connect( ipAdress,port)
                    .sync();
            log.info("客户端发送成功....");
            //发送消息
            future.channel().writeAndFlush(Unpooled.copiedBuffer(msg.toString(), CharsetUtil.UTF_8));
            // 等待连接被关闭
            future.channel().closeFuture().sync();
            return nettyClientHandler.getResponseResult();
        } catch (Exception e) {
            log.error("客户端Netty失败", e);
            throw new Exception("客户端Netty失败");
           // throw new Exception(CouponTypeEnum.OPERATE_ERROR.getCouponTypeDesc());
        } finally {
            //以一种优雅的方式进行线程退出
            group.shutdownGracefully();
        }

    }

}
