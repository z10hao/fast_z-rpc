package com.example.fastzrpc.netty;

import com.caucho.hessian.io.HessianInput;
import com.caucho.hessian.io.HessianOutput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * @author zhangt
 * @create 2022-09-12
 */
@Component
public class HessianSerializer  {
    private static final Logger logger = LoggerFactory.getLogger(HessianSerializer.class);

    public byte[] serialize(Object obj) throws Exception {
        HessianOutput hessianOutput = null;
        try(ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream()){
            hessianOutput = new HessianOutput(byteArrayOutputStream);
            hessianOutput.writeObject(obj);
            return byteArrayOutputStream.toByteArray();
        }catch (IOException e){
            logger.error("序列化时有错误发生" + e);
            throw new Exception("序列化时有错误发生");
        }finally {
            if(hessianOutput != null){
                try {
                    hessianOutput.close();
                }catch (IOException e){
                    logger.error("关闭output流时有错误发生" + e);
                }
            }
        }
    }

    public Object deserialize(byte[] bytes) throws Exception {
        HessianInput hessianInput = null;
        try (ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes)) {
            hessianInput = new HessianInput(byteArrayInputStream);
            return hessianInput.readObject();
        } catch (IOException e) {
            logger.error("反序列化时有错误发生" + e);
            throw new Exception("反序列化时有错误发生");
        } finally {
            if (hessianInput != null) {
                hessianInput.close();
            }
        }
    }
}
