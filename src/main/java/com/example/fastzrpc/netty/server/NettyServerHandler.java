package com.example.fastzrpc.netty.server;

import com.example.fastzrpc.dto.ChannelInfo;
import com.example.fastzrpc.handler.InvokeHandler;
import com.example.fastzrpc.netty.HessianSerializer;
import com.example.fastzrpc.utils.ByteUtil;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.Resource;
import java.nio.ByteBuffer;

/**
 * @author zhangt
 * @create 2022-09-11
 * netty服务端处理器
 */
@Slf4j
public class NettyServerHandler extends ChannelInboundHandlerAdapter {

    @Resource
    HessianSerializer hessianSerializer;
//    @Resource
//    InvokeHandler invokeHandler;

    ChannelInfo channelInfo;
    /**
     * 客户端连接会触发
     */
    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        log.info("Channel active......");
    }

    /**
     * 客户端发消息会触发
     */
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        log.info("服务器收到消息: {}", msg.toString());
//        ByteBuffer  in = (ByteBuffer) msg;
//        Object object=  hessianSerializer.deserialize(ByteUtil.bytebufferToByteArray(in));
        Object obj=  new InvokeHandler().invokeVerify(msg);
        ctx.writeAndFlush(obj);
    }


    /**
     * 发生异常触发
     */
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
        ctx.close();
    }

}
