package com.example.fastzrpc.Enum;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author zhangt
 * @create 2022-09-11
 */
@AllArgsConstructor
@Getter
public enum CouponTypeEnum {
    OPERATE_SUCCESS("0"),
    OPERATE_ERROR("-1");

    private final String couponTypeDesc;
}
