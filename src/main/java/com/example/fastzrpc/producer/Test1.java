package com.example.fastzrpc.producer;

import com.example.fastzrpc.annotation.RpcClass;
import com.example.fastzrpc.annotation.RpcService;
import com.example.fastzrpc.dto.UserDTO;
import com.example.fastzrpc.service.test01.Test01;

import java.util.Map;

/**
 * @author zhangt
 * @create 2022-09-10
 */
@RpcClass(group = "test")
public class Test1 {

    @RpcService
    public String helloRPC(String a, Map map, UserDTO userDTO){
          return "你好,fast_zRpc!";

    }

}
