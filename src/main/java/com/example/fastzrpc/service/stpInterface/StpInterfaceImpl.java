package com.example.fastzrpc.service.stpInterface;

import cn.dev33.satoken.stp.StpInterface;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zhangt
 * @create 2022-10-02
 */
@Component
public class StpInterfaceImpl implements StpInterface {

    @Override
    public List<String> getPermissionList(Object loginId, String loginType) {
        // 返回此 loginId 拥有的权限列表
        List<String> loList=new ArrayList<>();
        loList.add("123456");
        return loList;
    }

    @Override
    public List<String> getRoleList(Object loginId, String loginType) {
        List<String> loList=new ArrayList<>();
        loList.add("abc");
        return loList;
    }
}
