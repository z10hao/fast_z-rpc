package com.example.fastzrpc.service.test01;

import com.example.fastzrpc.annotation.RpcClass;
import org.springframework.stereotype.Service;

/**
 * @author zhangt
 * @create 2022-09-10
 */
@Service
public interface Test01 {
    void test01();
}
