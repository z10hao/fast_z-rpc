package com.example.fastzrpc.annotation;

import java.lang.annotation.*;

/**
    @author zhangt
    @create 2022-09-26
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
@Inherited
public @interface SaToken {

}
