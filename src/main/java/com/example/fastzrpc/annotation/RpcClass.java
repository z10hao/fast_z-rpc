package com.example.fastzrpc.annotation;

import org.springframework.stereotype.Component;

import java.lang.annotation.*;

/**
 * @author zhangt
 * @create 2022-09-10
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Inherited
@Component
public @interface RpcClass {
    /**
     * Service group, default value is empty string
     */
    String group() default "";
}
