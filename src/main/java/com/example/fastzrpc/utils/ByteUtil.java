package com.example.fastzrpc.utils;

import java.nio.ByteBuffer;

/**
 * @author zhangt
 * @create 2022-09-12
 */
public class ByteUtil {
    /**
     * byte 数组转byteBuffer
     * @param byteArray
     */
    public static ByteBuffer byteToByffer(byte[] byteArray) {

        //初始化一个和byte长度一样的buffer
        ByteBuffer buffer= ByteBuffer.allocate(byteArray.length);
        // 数组放到buffer中
        buffer.put(byteArray);
        //重置 limit 和postion 值 否则 buffer 读取数据不对
        buffer.flip();
        return buffer;
    }
    /**
     * byteBuffer 转 byte数组
     * @param buffer
     * @return
     */
    public static byte[] bytebufferToByteArray(ByteBuffer buffer) {
        //重置 limit 和postion 值
        buffer.flip();
        //获取buffer中有效大小
        int len=buffer.limit() - buffer.position();

        byte [] bytes=new byte[len];

        for (int i = 0; i < bytes.length; i++) {
            bytes[i]=buffer.get();

        }

        return bytes;
    }

}
