package com.example.fastzrpc.utils;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * @author zhangt
 * @create 2022-09-11
 */
public class IPUtil {
    public static String getIpAddress() {
        String ipAddress;
        try {
             ipAddress= InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            throw new RuntimeException(e);
        }

        return ipAddress;
    }
}
