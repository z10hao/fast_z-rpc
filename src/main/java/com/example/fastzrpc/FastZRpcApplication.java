package com.example.fastzrpc;

import cn.dev33.satoken.SaManager;
import cn.dev33.satoken.stp.StpUtil;
import com.example.fastzrpc.config.ZookeeperWatcher;
import com.example.fastzrpc.dto.ResponseResult;
import com.example.fastzrpc.handler.InvokeHandler;
import com.example.fastzrpc.netty.server.NettyServer;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.impl.StaticMDCBinder;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import javax.annotation.Resource;
import java.net.UnknownHostException;


@Slf4j
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})//关闭数据源自动配置功能
public class FastZRpcApplication extends SpringBootServletInitializer {

    public static void main(String[] args) throws UnknownHostException, InterruptedException {
        SpringApplication.run(FastZRpcApplication.class, args);
        //开启zk监听
        ZookeeperWatcher zookeeperWatcher = new ZookeeperWatcher();
        //开启Netty服务
        NettyServer nettyServer =new  NettyServer ();
        nettyServer.start();
        log.info("======服务已经启动========");
        System.out.println("启动成功：Sa-Token配置如下：" + SaManager.getConfig());
        //Result= invokeHandler.invokeDelivery("Test1","helloRPC","FAST");
    }
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(FastZRpcApplication.class);
    }

}
