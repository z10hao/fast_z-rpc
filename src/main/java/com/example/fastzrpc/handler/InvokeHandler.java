package com.example.fastzrpc.handler;

import com.alibaba.fastjson.JSONObject;
import com.example.fastzrpc.ZookeeperServerConfigProperties;
import com.example.fastzrpc.annotation.SaToken;
import com.example.fastzrpc.config.ServiceDiscoverConfig;
import com.example.fastzrpc.dto.ChannelInfo;
import com.example.fastzrpc.dto.ResponseResult;
import com.example.fastzrpc.dto.ServiceInstanceInfo;
import com.example.fastzrpc.netty.HessianSerializer;
import com.example.fastzrpc.netty.client.NettyClientUtil;
import com.example.fastzrpc.utils.SpringContextUtil;
import com.example.fastzrpc.utils.StringUtil;
import com.example.fastzrpc.utils.YamlReader;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.zookeeper.ZooKeeper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.io.IOException;
import java.lang.reflect.Method;

/**
 * @author zhangt
 * @create 2022-09-12
 * 调用本地方法
 */
@Slf4j
@Component
public class InvokeHandler {
    @Resource
    HessianSerializer hessianSerializer;
    @Resource
    ZookeeperServerConfigProperties zookeeperServerConfigProperties;
    ServiceInstanceInfo serviceInstanceInfo;
    ChannelInfo channelInfo;
    ResponseResult responseResult;
    @Value("${zRpc.application.timeout}")
    private  int SESSION_TIMEOUT;
    @Value("${zRpc.application.zookeeper}")
    private  String zkServers;
    @Autowired
    ApplicationContext applicationContext;

    public  Object invokeVerify(Object obj){
        try {
            ChannelInfo channelInfo= JSONObject.parseObject(obj.toString(),ChannelInfo.class);
            Object bean=SpringContextUtil.getBean(StringUtil.lowerFirst(
                    channelInfo.getBeanName().split("\\.")[channelInfo.getBeanName().split("\\.").length - 1]));
            Method[] methods = ReflectionUtils.getDeclaredMethods(bean.getClass());
            for (Method method :methods){
                if(method.getName().equals(channelInfo.getMethodName())){
                    Class<?>[] parameters = method.getParameterTypes();//参数类型数组
                    if(parameters.length==0){
                        Object  o = ReflectionUtils.invokeMethod(method, bean, null);
                        return o;
                    }else{
                        if (parameters.length!=channelInfo.getParameter().length)  continue;
                        //验证方法参数类型，以防重载
                        Object[] os= parametersVerify(parameters,channelInfo.getParameter());
                        if(os==null)  continue;
                        //反射执行方法
                        Object  o = ReflectionUtils.invokeMethod(method, bean, os);
                        return o;
                    }
                }
            }
            return "参数类型不匹配！" ;
        } catch (IllegalArgumentException e) {
            log.error("参数类型不匹配！");
            throw new RuntimeException(e);
        }
    }
    public Object[] parametersVerify(Class<?>[] parameters ,Object[] objects){
        try {
            Object[] objs=new Object[parameters.length];
            int a=0;
            for(Class<?> parameter : parameters ){
                ObjectMapper objectMapper= new ObjectMapper();
                objs[a]= objectMapper.convertValue(objects[a],parameter);
                a++;
            }
            return objs;
        } catch (Exception e) {
           return null;
        }
    }
    public  Object invokeDelivery(String groupBeanName,String methodName,Object... obj) throws Exception {
        responseResult=  NettyClientUtil.RpcNetty(hessianSerializer.serialize(getChannelInfo(groupBeanName,methodName,obj))
                ,serviceInstanceInfo.getIp(),serviceInstanceInfo.getPort());

        return  responseResult.getMsg();
    }
    @SaToken
    public ChannelInfo getChannelInfo(String groupBeanName,String methodName,Object... obj ) throws IOException {
        String groupName=groupBeanName.split("\\.")[0];
        String beanName=groupBeanName.split("\\.")[1];
        ZooKeeper zooKeeper = new ZooKeeper(zkServers,SESSION_TIMEOUT,(watchedEvent) -> {});
        ServiceDiscoverConfig serviceDiscoverConfig=new ServiceDiscoverConfig(zookeeperServerConfigProperties,zooKeeper);
        serviceInstanceInfo= serviceDiscoverConfig.getRegistry().get(groupName).get(beanName).get(methodName);
        channelInfo=new ChannelInfo();
        channelInfo.setBeanName(beanName);
        channelInfo.setMethodName(methodName);
        channelInfo.setParameter(obj);
        channelInfo.setGroupName(groupName);
        return channelInfo;
    }
}
