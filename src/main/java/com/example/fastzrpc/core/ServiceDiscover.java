package com.example.fastzrpc.core;

import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.WatchedEvent;

/**
 * @author zhangt
 * @create 2022-09-11
 */
public interface ServiceDiscover {
    void discovery(WatchedEvent e) throws KeeperException, InterruptedException;
}
