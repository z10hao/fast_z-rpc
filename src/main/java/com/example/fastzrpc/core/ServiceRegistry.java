package com.example.fastzrpc.core;

/**
 * @author zhangt
 * @create 2022-09-10
 */
public interface ServiceRegistry {
    /**
     * 注册服务信息
     *
     * @param serviceName    服务名称
     * @param beanGroup 群组
     *
     */
    void register(String serviceName,String beanGroup,String BeanName,String beanNameFull,Class<?>[] parameterTypes);

    void register(String[] nodes,Class<?>[] parameterTypes);
}
