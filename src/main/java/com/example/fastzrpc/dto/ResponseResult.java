package com.example.fastzrpc.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author zhangt
 * @create 2022-09-11
 */
@Getter
@Setter
@ToString
public class ResponseResult implements Serializable {
    private static final long serialVersionUID = 1L;

    @Getter
    @Setter
    private String code;

    @Getter
    @Setter
    private Object msg;

    public static  ResponseResult success( Object msg,String code) {
        return restResult(msg,code);
    }
    private static  ResponseResult restResult( Object msg,String code) {
        ResponseResult apiResult = new ResponseResult();
        apiResult.setCode(code);
        apiResult.setMsg(msg);
        return apiResult;
    }


}
