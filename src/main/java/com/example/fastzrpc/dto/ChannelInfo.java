package com.example.fastzrpc.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Map;

/**
 * @author zhangt
 * @create 2022-09-12
 */
@Data
public class ChannelInfo implements Serializable {
    private String groupName;
    private String beanName;

    private String methodName;

    private Object[] parameter;
    private Map<String,Object> map;

}
