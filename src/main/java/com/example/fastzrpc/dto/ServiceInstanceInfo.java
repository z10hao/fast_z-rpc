package com.example.fastzrpc.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author zhangt
 * @create 2022-09-11
 */
@Data
public class ServiceInstanceInfo implements Serializable {
    private String serviceName;

    private Class<?>[] parameterTypes;
    private String beanNameFull;
    private String ip;
    private int port;


    public ServiceInstanceInfo(String serviceName, String ip, int port,String beanNameFull,Class<?>[] parameterTypes) {
        this.serviceName = serviceName;
        this.ip = ip;
        this.port = port;
        this.beanNameFull = beanNameFull;
        this.parameterTypes = parameterTypes;
    }


}
