package com.example.fastzrpc.dto;

import java.io.Serializable;
import java.util.List;

/**
 * @author zhangt
 * @create 2022-09-10
 */
public class RpcBeanInfoDTO implements Serializable {
    private  String beanName;
    private  String beanGroup;
    private List<RpcMethodInfoDTO> beanMethods;

    public String getBeanName() {
        return beanName;
    }

    public void setBeanName(String beanName) {
        this.beanName = beanName;
    }

    public String getBeanGroup() {
        return beanGroup;
    }

    public void setBeanGroup(String beanGroup) {
        this.beanGroup = beanGroup;
    }

    public List<RpcMethodInfoDTO> getBeanMethods() {
        return beanMethods;
    }

    public void setBeanMethods(List<RpcMethodInfoDTO> beanMethods) {
        this.beanMethods = beanMethods;
    }
}
