package com.example.fastzrpc.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author zhangt
 * @create 2022-09-10
 */
@Data
public class RpcMethodInfoDTO implements Serializable {

    private  String methodName;

    private Class<?>[] parameterTypes;


}
