package com.example.fastzrpc.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author zhangt
 * @create 2022-09-14
 */
@Data
public class UserDTO implements Serializable {
    private String name;
    private Integer age;
}
