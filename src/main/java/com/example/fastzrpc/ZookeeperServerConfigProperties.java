package com.example.fastzrpc;

import com.example.fastzrpc.utils.YamlReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author zhangt
 * @create 2022-09-11
 */
@Component
public class ZookeeperServerConfigProperties {
    private static final Logger logger = LoggerFactory.getLogger(ZookeeperServerConfigProperties.class);
    private static String serverAddress;
    private static Integer sessionTimeout=2000;
    private static String namespace;
    private static String zRpcPort;
    private static String serverAddrKey="zRpc.application.zookeeper";
    private static String sessionTimeoutKey="zRpc.application.timeout";
    private static String namespaceKey="zRpc.application.name";
    private static String zRpcPortKey="zRpc.protocol.port";
    public static final ZookeeperServerConfigProperties configPropertiesHolder = new ZookeeperServerConfigProperties();
    private static String confFile="application.yml";
    static {
        logger.info("开始从"+confFile+"加载zookeeper配置.");
        try (InputStream ins = ZookeeperServerConfigProperties.class.getClassLoader()
                .getResourceAsStream(confFile)) {
            if (ins == null)
                throw new Exception("配置错误，在classpath下未发现'" + confFile + "文件！");
            Properties properties = new Properties();
            properties.load(ins);
            String addr = YamlReader.instance.getValueByKey(serverAddrKey).toString();
            if (addr == null || "".equals(addr))
                logger.warn("配置错误，'" + serverAddrKey + "'未配置！");
            else
                configPropertiesHolder.serverAddress = addr;
            Integer timeout = Integer.parseInt(YamlReader.instance.getValueByKey(sessionTimeoutKey).toString());
            if (timeout==0) {
                configPropertiesHolder.sessionTimeout = 2000;
            }
            else {
                try {
                    configPropertiesHolder.sessionTimeout = timeout;
                }
                catch (NumberFormatException e) {
                    logger.warn("配置错误，'" + sessionTimeoutKey + "'值不合法！");
                }
            }
            String ns ="/"+YamlReader.instance.getValueByKey(namespaceKey).toString();
            if (ns == null || "".equals(ns)) {
                logger.warn("配置错误，'" + namespaceKey + "'未配置！");
            }
            else {
                configPropertiesHolder.namespace = ns;
            }
            String zp =  YamlReader.instance.getValueByKey(zRpcPortKey).toString();
            if (zp == null || "".equals(zp)) {
                logger.warn("配置错误，'" + zRpcPortKey + "'未配置！");
            }
            else {
                configPropertiesHolder.zRpcPort = zp;
            }
            logger.info("从" + confFile + "读取zookeeper配置完成.");
        }
        catch (IOException e) {
            logger.error("配置错误," + e.getMessage());
            System.exit(1);
        }
        catch (Exception e) {
            logger.error(e.getMessage());
            System.exit(1);
        }
    }

    private ZookeeperServerConfigProperties() {
    }

    public static ZookeeperServerConfigProperties config() {
        return configPropertiesHolder;
    }

    public String getServerAddress() {
        return serverAddress;
    }

    public void setServerAddress(String serverAddress) {
        this.serverAddress = serverAddress;
    }

    public Integer getSessionTimeout() {
        return sessionTimeout;
    }

    public void setSessionTimeout(Integer sessionTimeout) {
        this.sessionTimeout = sessionTimeout;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public String getzRpcPort() {
        return zRpcPort;
    }

    public void setzRpcPort(String zRpcPort) {
        this.zRpcPort = zRpcPort;
    }
}
