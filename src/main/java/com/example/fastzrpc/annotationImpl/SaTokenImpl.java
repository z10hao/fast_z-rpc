package com.example.fastzrpc.annotationImpl;

import cn.dev33.satoken.id.SaIdUtil;
import com.alibaba.fastjson.JSON;
import com.example.fastzrpc.annotation.SaToken;
import com.example.fastzrpc.dto.ChannelInfo;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author zhangt
 * @create 2022-09-29
 */
@Aspect
@Component
@Slf4j
public class SaTokenImpl {

    @Around("@annotation(saToken)")
    public Object around(ProceedingJoinPoint joinPoint, SaToken saToken) throws Throwable {
        Object o = joinPoint.proceed();
        ChannelInfo c = JSON.parseObject(JSON.toJSONString(o), ChannelInfo.class);
        c.getMap().put(SaIdUtil.ID_TOKEN, SaIdUtil.getToken());
        return (Object)c;
    }
}
