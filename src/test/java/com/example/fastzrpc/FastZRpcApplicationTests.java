package com.example.fastzrpc;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.example.fastzrpc.dto.ChannelInfo;
import com.example.fastzrpc.dto.ResponseResult;
import com.example.fastzrpc.dto.UserDTO;
import com.example.fastzrpc.handler.InvokeHandler;
import com.example.fastzrpc.netty.HessianSerializer;
import com.example.fastzrpc.netty.client.NettyClient;
import com.example.fastzrpc.netty.client.NettyClientUtil;
import com.example.fastzrpc.producer.Test1;
import org.junit.jupiter.api.Test;
import org.redisson.Redisson;
import org.redisson.api.RFuture;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.Http2;

import javax.annotation.Resource;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

@SpringBootTest
class FastZRpcApplicationTests {
    @Resource
    InvokeHandler invokeHandler;
    ResponseResult Result;
    @Resource
    HessianSerializer hessianSerializer;
    ChannelInfo channelInfo;
    @Test
    void contextLoads() throws Exception {
        Object[] o=new Object[3];
        o[0]="FAST";
        Map<String,String> map=new ConcurrentHashMap<>();
        map.put("cs","ppo");
        o[1]=map;
        UserDTO userDTO=  new UserDTO();
        userDTO.setName("zt");
        userDTO.setAge(24);
        o[2]=userDTO;
        channelInfo=new ChannelInfo();
        channelInfo.setBeanName("com.example.fastzrpc.producer.Test1");
        channelInfo.setMethodName("helloRPC");
        channelInfo.setParameter(o);
        channelInfo.setGroupName("test");
        Result=  NettyClientUtil.RpcNetty(JSON.toJSONString(channelInfo),"10.0.38.131",20980);
        System.out.println("终于tcp");
    }

    void  redisLockTest() throws InterruptedException {
        RedissonClient redis= Redisson.create();
        RLock rLock=redis.getLock("resoucenname");
        //直接加锁
        rLock.lock();
        //尝试加锁五秒，锁过期时间十秒
        rLock.tryLock(5,10, TimeUnit.SECONDS);
        //支持非阻塞异步操作
        RFuture<Boolean> rFuture=rLock.tryLockAsync(5,10,TimeUnit.SECONDS);
        rFuture.whenCompleteAsync((Result,Throwable)->{
            System.out.println("当前加锁的情况："+Result+Throwable);
        });
        //解锁
        rLock.unlock();

    }

}
